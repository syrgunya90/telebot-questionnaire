import configparser
import json
import os
import telebot
import psycopg2
import csv
import urllib.request
from psycopg2 import sql
from telebot import types
from contextlib import closing
config = configparser.ConfigParser()  # создаём объекта парсера
config.read("config.ini")  # читаем конфиг
bot = telebot.TeleBot(config["Bot"]["token"])


def get_reports(interval):
    with closing(psycopg2.connect("dbname=" + config["Postgres"]["dbname"] + " "
                                                                             "user=" + config["Postgres"][
                                      "username"] + " "
                                                    "password=" + config["Postgres"]["password"] + " "
                                                                                                   "host=" +
                                  config["Postgres"]["host"] + " "
                                                               "port=" + config["Postgres"]["port"])) as conn:
        with conn.cursor() as cursor:
            cursor.execute("select id, question_id, tg_user_id, tg_user_name, question_type, answer, to_char("
                           "answ_date, 'DD-MM-YYYY HH12:MI:SS') from answers where answ_date >= Now() - interval %s "
                           "order by id",
                           (interval,))
            rep = []
            rep_row = []
            for row in cursor:
                rep_row.append(row[0])
                rep_row.append(row[1])
                rep_row.append(row[2])
                rep_row.append(row[3])
                rep_row.append(row[4])
                if row[4] == 3:
                    url_download_file = 'https://api.telegram.org/file/bot{0}/{1}'
                    url_get_file_path = 'https://api.telegram.org/bot{0}/getFile?file_id={1}'.format(config["Bot"]["token"], row[5])
                    with urllib.request.urlopen(url_get_file_path) as res:
                        data = json.loads(
                            res.read().decode())
                    rep_row.append(url_download_file.format(config["Bot"]["token"], data['result']['file_path']))
                else:
                    rep_row.append(row[5])
                rep_row.append(' '+row[6])
                rep.append(rep_row)
                rep_row = []
    with open("report.csv", "w", newline="") as file:
        writer = csv.writer(file)
        writer.writerows(rep)


def get_question(ids):
    with closing(psycopg2.connect("dbname=" + config["Postgres"]["dbname"] + " "
                                                                             "user=" + config["Postgres"][
                                      "username"] + " "
                                                    "password=" + config["Postgres"]["password"] + " "
                                                                                                   "host=" +
                                  config["Postgres"]["host"] + " "
                                                               "port=" + config["Postgres"]["port"])) as conn:
        with conn.cursor() as cursor:
            cursor.execute("select id, question_text, question_type, answer_types, attachment from questions where id "
                           "=  %s", (ids,))
            for row in cursor:
                return {
                    "q_id": row[0],
                    "q_text": row[1],
                    "q_type": row[2],
                    "a_type": row[3],
                    "q_attachment": row[4]
                }

            return None


def get_last_answer_id(user_ids):
    with closing(psycopg2.connect("dbname=" + config["Postgres"]["dbname"] + " "
                                                                             "user=" + config["Postgres"][
                                      "username"] + " "
                                                    "password=" + config["Postgres"]["password"] + " "
                                                                                                   "host=" +
                                  config["Postgres"]["host"] + " "
                                                               "port=" + config["Postgres"]["port"])) as conn:
        with conn.cursor() as cursor:
            cursor.execute("select question_id from answers where tg_user_id = %s and answ_date = (select max("
                           "answ_date) from answers where tg_user_id = %s)", (user_ids, user_ids,))
            for row in cursor:
                return {
                    "q_id": row[0]
                }

            return None


def delete_answers_by_tg_id(tg_id):
    with closing(psycopg2.connect("dbname=" + config["Postgres"]["dbname"] + " "
                                                                             "user=" + config["Postgres"][
                                      "username"] + " "
                                                    "password=" + config["Postgres"]["password"] + " "
                                                                                                   "host=" +
                                  config["Postgres"]["host"] + " "
                                                               "port=" + config["Postgres"]["port"])) as conn:
        with conn.cursor() as cursor:
            conn.autocommit = True
            cursor.execute("delete from answers where tg_user_id = %s ", (tg_id,))


def insert_answer(question_id, tg_user_id, tg_user_name, question_type, answer):
    with closing(psycopg2.connect("dbname=" + config["Postgres"]["dbname"] + " "
                                                                             "user=" + config["Postgres"][
                                      "username"] + " "
                                                    "password=" + config["Postgres"]["password"] + " "
                                                                                                   "host=" +
                                  config["Postgres"]["host"] + " "
                                                               "port=" + config["Postgres"]["port"])) as conn:
        with conn.cursor() as cursor:
            conn.autocommit = True
            values = [
                (question_id, tg_user_id, tg_user_name, question_type, answer, 'now()'),
            ]
            insert = sql.SQL('INSERT INTO answers (question_id, tg_user_id, tg_user_name, question_type, answer, '
                             'answ_date) VALUES {}').format(
                sql.SQL(',').join(map(sql.Literal, values))
            )
            cursor.execute(insert)


def set_question(message, query, wrong_answer):
    if query is not None:
        if wrong_answer == 1:
            if query["q_type"] == 1:
                bot.send_message(message.chat.id, "Формат ответа должен быть текст")
            else:
                bot.send_message(message.chat.id, "Формат ответа должен быть " + query["a_type"])
        if query["q_attachment"] is not None and os.path.exists(
                config["Attachment"]["path"] + query["q_attachment"]) and os.path.isfile(
            config["Attachment"]["path"] + query["q_attachment"]):
            uis_file = open(config["Attachment"]["path"] + query["q_attachment"], 'rb')
            bot.send_document(message.chat.id, uis_file)
            uis_file.close()
        if query["q_type"] == 2:
            keyboard1 = types.ReplyKeyboardMarkup(True, True)
            answ = query["a_type"].split("|")
            for element in answ:
                keyboard1.row(element)
            bot.send_message(message.chat.id, query["q_text"], reply_markup=keyboard1)
        elif query["q_type"] == 3:
            bot.send_message(message.chat.id, query["q_text"] + " " + query["a_type"])
        else:
            bot.send_message(message.chat.id, query["q_text"])
    else:
        bot.send_message(message.chat.id, "Вы ответили на все вопросы.")


def set_question_admin(message):
    keyboard1 = types.ReplyKeyboardMarkup(True, True)
    keyboard1.row('1 сутки')
    keyboard1.row('7 суток')
    keyboard1.row('30 суток')
    bot.send_message(message.chat.id, "Выберите период отчета", reply_markup=keyboard1)


def get_file_id_size(message):
    if message.content_type == 'audio':
        return {
            "file_id": message.audio.file_id,
            "file_size": message.audio.file_size
        }
    elif message.content_type == 'document':
        return {
            "file_id": message.document.file_id,
            "file_size": message.document.file_size
        }
    elif message.content_type == 'photo':
        return {
            "file_id": message.photo[0].file_id,
            "file_size": message.photo[0].file_size
        }
    elif message.content_type == 'video':
        return {
            "file_id": message.video.file_id,
            "file_size": message.video.file_size
        }
    elif message.content_type == 'video_note':
        return {
            "file_id": message.video_note.file_id,
            "file_size": message.video_note.file_size
        }
    elif message.content_type == 'voice':
        return {
            "file_id": message.voice.file_id,
            "file_size": message.voice.file_size
        }
    else:
        return {
            "file_id": -1,
            "file_size": -1
        }


@bot.message_handler(commands=['start', 'new'])
def start_message(message):
    if message.from_user.id == int(config["Admin"]["tgId"]):
        if message.text == '/start':
            set_question_admin(message)
    else:
        if message.text == '/start':
            bot.send_message(message.chat.id, 'Привет, это опросник. Чтобы начать опрос заново отправь /new')
            last_answer = get_last_answer_id(message.from_user.id)
            if last_answer is not None:
                query = get_question(last_answer['q_id'] + 1)
            else:
                query = get_question(1)
            if query is not None:
                bot.send_message(message.chat.id, 'Вы ответили не на все вопросы.')
                set_question(message, query, 0)
            else:
                bot.send_message(message.chat.id, "Вы уже ответили на все вопросы.")
        else:
            delete_answers_by_tg_id(message.from_user.id)
            query = get_question(1)
            if query is not None:
                set_question(message, query, 0)
            else:
                bot.send_message(message.chat.id, "нет актуальных вопросов. Попробуйте через некоторое время")


@bot.message_handler(content_types=['text'])
def get_text_messages(message):
    if message.from_user.id == int(config["Admin"]["tgId"]):
        interval = None
        if message.text == '1 сутки':
            interval = '1 day'
        elif message.text == '7 суток':
            interval = '7 days'
        elif message.text == '30 суток':
            interval = '30 days'
        if interval is not None:
            get_reports(interval)
            if (os.path.exists('report.csv') and os.path.isfile(
                    'report.csv')) and os.stat('report.csv').st_size > 0:
                uis_file = open('report.csv', 'rb')
                bot.send_document(message.chat.id, uis_file)
                uis_file.close()
            else:
                bot.send_message(message.chat.id, "За указанный период нет ответов")
                set_question_admin(message)
        else:
            bot.send_message(message.chat.id, "Не верный формат")
            set_question_admin(message)
    else:
        last_answer = get_last_answer_id(message.from_user.id)
        if last_answer is not None:
            query = get_question(last_answer['q_id'] + 1)
        else:
            query = get_question(1)
        if query is not None:
            if query["q_type"] == 2:
                answer_types = query["a_type"].split("|")
                need_format = False
                for element in answer_types:
                    if element == message.text:
                        need_format = True
                        break
                if need_format:
                    insert_answer(query["q_id"], message.from_user.id, message.from_user.username, query["q_type"],
                                  message.text)
                    query = get_question(query["q_id"] + 1)
                    set_question(message, query, 0)
                else:
                    set_question(message, query, 1)
            elif query["q_type"] == 1:
                answer_text = message.text[0:5000]
                insert_answer(query["q_id"], message.from_user.id, message.from_user.username, query["q_type"],
                              answer_text)
                query = get_question(query["q_id"] + 1)
                set_question(message, query, 0)
            else:
                set_question(message, query, 1)
        else:
            bot.send_message(message.chat.id, "Вы уже ответили на все вопросы, чтобы начать заново отправьте /new")


@bot.message_handler(content_types=['audio', 'document', 'photo', 'video', 'video_note', 'voice'])
def get_doc_messages(message):
    if message.from_user.id != int(config["Admin"]["tgId"]):
        last_answer = get_last_answer_id(message.from_user.id)
        if last_answer is not None:
            query = get_question(last_answer['q_id'] + 1)
        else:
            query = get_question(1)
        if query is not None:
            if query["q_type"] == 3:
                answer_types = query["a_type"].split("|")
                need_format = False
                for element in answer_types:
                    if element == message.content_type or element == 'any':
                        need_format = True
                        break
                if need_format:
                    file_id_and_size = get_file_id_size(message)
                    if file_id_and_size["file_size"] != -1 and file_id_and_size["file_size"] < int(
                            config["File"]["maxSize"]):
                        insert_answer(query["q_id"], message.from_user.id, message.from_user.username, query["q_type"],
                                      file_id_and_size["file_id"])
                        query = get_question(query["q_id"] + 1)
                        set_question(message, query, 0)
                    else:
                        bot.send_message(message.chat.id,
                                         "Объем фала не должен  превышать " + config["File"]["maxSize"] + " МБ")
                        set_question(message, query, 1)
                else:
                    set_question(message, query, 1)
            else:
                set_question(message, query, 1)
        else:
            bot.send_message(message.chat.id, "Вы уже ответили на все вопросы, чтобы начать заново отправьте /new")


@bot.message_handler(content_types=['sticker', 'location', 'contact'])
def get_wrong_messages(message):
    if message.from_user.id != int(config["Admin"]["tgId"]):
        last_answer = get_last_answer_id(message.from_user.id)
        if last_answer is not None:
            query = get_question(last_answer['q_id'] + 1)
        else:
            query = get_question(1)
        if query is not None:
            set_question(message, query, 1)
        else:
            bot.send_message(message.chat.id, "Вы уже ответили на все вопросы, чтобы начать заново отправьте /new")


bot.polling()
